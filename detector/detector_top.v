`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:        Politechnika Warszawska
// Engineer:       Magdalena Dzialak
// 
// Create Date:    20:47:13 07/25/2016 
// Design Name: 
// Module Name:    detector_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "detector_parameters.vh"


module detector_top(

  input        clk_board,
	 
		// control signals
		input        alert,
		output reg   rel_n_on,
	 output reg   rel_p_on,
		output reg   dcu_disable,
		
		// leds
		output       led_ctrl,
		output       led_rel_n_on,
		output       led_rel_p_on,
		
		// UART
  input        uart_rx,
  output       uart_tx,
		
		// I2C sensors
		inout        i2c_clk,
		inout        i2c_data

);

//////////////////////////////////////////////////////////////////////////////////
// Signals 
wire        clk;
reg         alert_en = 1'b0;
//////////////////////////////////////////////////////////////////////////////////
// Signals used to connect KCPSM3 to program ROM and I/O logic
reg  	     	interrupt;
reg   [7:0] in_port;
//
wire  [9:0]	address;
wire [17:0]	instruction;
wire  [7:0]	port_id;
wire  [7:0] out_port;
wire       	write_strobe;
wire  	     read_strobe;
wire  	     interrupt_ack;
//////////////////////////////////////////////////////////////////////////////////
// Signals for connection of peripherals
wire [7:0] 	uart_status_port;
//////////////////////////////////////////////////////////////////////////////////
// Signals to form an timer generating an interrupt every microsecond
reg   [5:0] micro_sec_counter;
reg   [9:0] mili_sec_counter;
reg   [2:0] sec_counter;
reg  		     interrupt_event;
//////////////////////////////////////////////////////////////////////////////////
// Signals for I2C connections
reg         drive_i2c_clk;
reg         drive_i2c_data;
//////////////////////////////////////////////////////////////////////////////////
// Signals for UART connections
reg   [9:0] baud_count;
reg  	     	en_16_x_baud;
reg  	     	read_from_uart;
//
wire  	     write_to_uart;
wire  	     tx_full;
wire  	     tx_half_full;
wire  [7:0] rx_data;
wire       	rx_data_present;
wire       	rx_full;
wire       	rx_half_full;

//////////////////////////////////////////////////////////////////////////////////
// DEBUG REGS  ///////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
(* KEEP = "TRUE" *) reg   [9:0] address_dbg;
(* KEEP = "TRUE" *) reg  [17:0] instruction_dbg;
(* KEEP = "TRUE" *) reg   [7:0] port_id_dbg;
(* KEEP = "TRUE" *) reg         write_strobe_dbg;
(* KEEP = "TRUE" *) reg   [7:0] out_port_dbg;
(* KEEP = "TRUE" *) reg         read_strobe_dbg;
(* KEEP = "TRUE" *) reg   [7:0] in_port_dbg;
(* KEEP = "TRUE" *) reg         i2c_clk_dbg;
(* KEEP = "TRUE" *) reg         i2c_data_dbg;
(* KEEP = "TRUE" *) reg         drive_i2c_clk_dbg;
(* KEEP = "TRUE" *) reg         drive_i2c_data_dbg;
	
always @ (posedge clk)
    begin
								address_dbg        <= address;
								instruction_dbg    <= instruction;
								port_id_dbg        <= port_id;
								write_strobe_dbg   <= write_strobe;
								out_port_dbg       <= out_port;
								read_strobe_dbg    <= read_strobe;
								in_port_dbg        <= in_port;
								i2c_clk_dbg        <= i2c_clk;
								i2c_data_dbg       <= i2c_data;
								drive_i2c_clk_dbg  <= drive_i2c_clk;
								drive_i2c_data_dbg <= drive_i2c_data;
				end
		
//////////////////////////////////////////////////////////////////////////////////
// LEDS  /////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////		
assign led_ctrl     = 1'b1;
assign led_rel_n_on = rel_n_on;
assign led_rel_p_on = rel_p_on;



//////////////////////////////////////////////////////////////////////////////////
// INTERRUPTS  ///////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// Interrupt is a generated once every 50 clock cycles to provide a 1us reference. 
// Interrupt is automatically cleared by interrupt acknowledgment from KCPSM3.
always @(posedge clk)
    begin
								///////////////////////////////////////////////////////////////////
								// interrupt counters
								if (micro_sec_counter == 6'd49)
								    micro_sec_counter <= 6'd0;
								else
								    micro_sec_counter <= micro_sec_counter + 6'd1;
												
								if (mili_sec_counter == 10'd999)
								    mili_sec_counter <= 10'd0;
								else
								    mili_sec_counter <= mili_sec_counter + 16'd1;
								
				    ///////////////////////////////////////////////////////////////////
								// interrupt event								
								if (sec_counter == 3'd2)
								    begin
								        sec_counter     <= 3'd0;
											    	interrupt_event <= 1'b1;
												end
								else
								    begin
								        sec_counter     <= sec_counter + 3'd1;
																interrupt_event <= 1'b0;
												end

								///////////////////////////////////////////////////////////////////
								// interrupt
								if (interrupt_ack)
							    	interrupt <= 1'b0;
								else if (interrupt_event) 
							    	interrupt <= 1'b1;
								else
            interrupt <= interrupt;
				end

//////////////////////////////////////////////////////////////////////////////////
// CONTROL SIGNALS ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
always @ (posedge clk)
//if (rst)
//    dcu_disable <= 1'b1;
//else 
if (alert_en && alert)
    dcu_disable <= 1'b0;


//////////////////////////////////////////////////////////////////////////////////
// KCPSM3 INPUT PORTS  ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// UART FIFO status signals to form a bus
assign uart_status_port = {3'b 000,rx_data_present,rx_full,rx_half_full,tx_full,tx_half_full};

// The inputs connect via a pipelined multiplexer
always @ (posedge clk) 
    begin
        case (port_id)
																			                   // read UART status at address 00 hex
												`PORT_ID_UART_STATUS:     in_port <= uart_status_port;
																			                   // read UART receive data at address 01 hex
												`PORT_ID_UART_DATA:       in_port <= rx_data;
												`PORT_ID_I2C_INPUT:    begin
												                          in_port[0] <= i2c_clk;
																									            	in_port[1] <= i2c_data;
																						             end
												
												default:                  in_port <= 8'b XXXXXXXX;
        endcase

								read_from_uart <= read_strobe && (port_id == `PORT_ID_UART_DATA);
    end

//////////////////////////////////////////////////////////////////////////////////
// KCPSM3 OUTPUT PORTS  //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// write to UART transmitter FIFO buffer at address 01 hex.
// This is a combinatorial decode because the FIFO is the 'port register'.
//
assign write_to_uart = write_strobe && (port_id == `PORT_ID_UART_DATA);

// adding the output registers to the clock processor
// Alarm register at address 00 hex with data bit0 providing control
always @ (posedge clk) 
//    if (rst)
//				    begin
//            alert_en <= 1'b0;
//								    rel_n_on <= 1'b0;
//												rel_p_on <= 1'b1;
//								end
//   	else 
				if (write_strobe)
        begin
								    if (port_id == `PORT_ID_I2C_OUTPUT)
													   begin
																	    drive_i2c_clk  <= out_port[0];
                     drive_i2c_data <= out_port[1];
																end
												else if (port_id == `PORT_ID_CONTROL_COMMAND)
												    begin
																    case (out_port)
																				    `UART_CMD_ALERT_EN:        alert_en <= 1'b1;   
																				    `UART_CMD_REL_N_P_ON:  begin
																								                           rel_n_on <= 1'b1;
																																																		 rel_p_on <= 1'b1;
																								                       end
																								`UART_CMD_REL_N_P_OFF: begin
																								                           rel_n_on <= 1'b0;
																																																		 rel_p_on <= 1'b0;
																								                       end
																			    	`UART_CMD_REL_N_ON:        rel_n_on <= 1'b1;
																								`UART_CMD_REL_N_OFF:       rel_n_on <= 1'b0;
																								`UART_CMD_REL_P_ON:        rel_p_on <= 1'b1;
																								`UART_CMD_REL_P_OFF:       rel_p_on <= 1'b0;
																				    default: ;
																				endcase
																end
        end								
				    

//////////////////////////////////////////////////////////////////////////////////
// BUFG   ////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
BUFG BUFG_inst(
	.O ( clk ),           // Clock buffer output
	.I ( clk_board )      // Clock buffer input
);

//////////////////////////////////////////////////////////////////////////////////
// KCPSM3  ///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
kcpsm3 processor(
	 .address       ( address ),
		.instruction   ( instruction ),
		.port_id       ( port_id ),
		.write_strobe  ( write_strobe ),
		.out_port      ( out_port ),
		.read_strobe   ( read_strobe ),
		.in_port       ( in_port ),
		.interrupt     ( interrupt ),
		.interrupt_ack ( interrupt_ack ),
		.reset         ( 1'b0 ),
		.clk           ( clk )
);

//////////////////////////////////////////////////////////////////////////////////
// PROGRAM MEMORY  ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
program_ROM program_ROM(
 	.address     ( address ),
  .instruction ( instruction ),
  .clk         ( clk )
);


//////////////////////////////////////////////////////////////////////////////////
// I2C  //////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//'i2c_clk' and 'i2c_data' are open collector bidirectional pins.
//
// When KCPSM6 presents a '0' to either of the 'drive' signals the corresponding pin 
// will be forced Low.
//   
// When KCPSM6 presents a '1' to either of the 'drive' signals the corresponding pin 
// will become high impedance ('Z') allowing the signal to be pulled High by the 
// external pull-up or driven Low by a Slave device.
assign i2c_clk  = (drive_i2c_clk == 1'b0) ? 1'b0 : 1'bZ;
assign i2c_data = (drive_i2c_data == 1'b0) ? 1'b0 : 1'bZ;

		
//////////////////////////////////////////////////////////////////////////////////
// UART  /////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
// Connect the 8-bit, 1 stop-bit, no parity transmit and receive macros.
// Each contains an embedded 16-byte FIFO buffer.
//
//////////////////////////////////////////////////////////////////////////////////
// baud rate for UART 
always @ (posedge clk) 
				if (baud_count == 81) // 50 MHz, 38400 baud rate
								begin
											baud_count   <= 1'b0;
											en_16_x_baud <= 1'b1;
								end
				else
							begin
											baud_count   <= baud_count + 1;
											en_16_x_baud <= 1'b0;
							end
							
//////////////////////////////////////////////////////////////////////////////////
// uart_tx
uart_tx transmit(	
		.data_in          ( out_port ),
		.write_buffer     ( write_to_uart ),
		.reset_buffer     ( 1'b0 ),
		.en_16_x_baud     ( en_16_x_baud ),
		.serial_out       ( uart_tx ),
		.buffer_full      ( tx_full ),
		.buffer_half_full ( tx_half_full ),
		.clk              ( clk )
);
//////////////////////////////////////////////////////////////////////////////////
// uart_rx
uart_rx receive(	
		.serial_in           ( uart_rx ),
		.data_out            ( rx_data ),
		.read_buffer         ( read_from_uart ),
		.reset_buffer        ( 1'b0 ),
		.en_16_x_baud        ( en_16_x_baud ),
		.buffer_data_present ( rx_data_present ),
		.buffer_full         ( rx_full ),
		.buffer_half_full    ( rx_half_full ),
		.clk                 ( clk )
);
				

endmodule


