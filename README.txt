Wersja oprogramowania: ISE 13.3 (nt64), iMPACT 13.3 (nt64)

///////////////////////////////////////////////////////////////////////


Termite - ustawienia:

Port configuration
baud rate: 38400
data bits: 8
stop bits: 1
parity: none
flow control: none

Transmitted text
Append CR
unchecked Local echo

Plug-ins
Hex View - enable entry of hexadecimal values


///////////////////////////////////////////////////////////////////////
Kompilator asemblera KCPSM3

W repozytorium pod �cie�k� /detector_assembler/kcpsm3.exe umieszczony jest kompilator,
nale�y go uruchomi� i wpisa� nazw� odpowiedniego pliku .psm: program ROM.
Wygenerowany zostaje plik program_ROM.v, kt�rego zawarto�� jest automatycznie uaktualniania
w projekcie w ISE.

///////////////////////////////////////////////////////////////////////

Struktura repozytorium:
detector                - projekt w ISE wraz z plikami �r�d�owymi
detector_assembler      - pliki oraz kompilator asemblera
detector_doc            - NIEISTOTNE, tekst pracy in�ynierskiej, notatki
